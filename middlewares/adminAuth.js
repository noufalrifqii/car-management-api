const jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");

const roleAuthSuperAdmin = (req, res, next) => {
  try {
    const authHeader = req.headers["authorization"];
    if (authHeader) {
      // const jwtKey = process.env.JWT_KEY;
      const token = authHeader.split(" ")[1];
      const decoded = jwt.decode(token);
      let role = decoded.role;
      if (role == 0) {
        next();
      } else {
        res.status(401).json({
          message: "MAAF ANDA BUKAN SUPER ADMIN!",
        });
      }
      console.log(decoded);
    } else {
      res.sendStatus(401);
    }
  } catch (error) {}
};

const roleAuth = (req, res, next) => {
  try {
    const authHeader = req.headers["authorization"];
    if (authHeader) {
      // const jwtKey = process.env.JWT_KEY;
      const token = authHeader.split(" ")[1];
      const decoded = jwt.decode(token);
      let role = decoded.role;
      if (role == 0 || role == 1) {
        next();
      } else {
        res.status(401).json({
          message: "MAAF ANDA BUKAN ADMIN!",
        });
      }
      console.log(decoded);
    } else {
      res.sendStatus(401);
    }
  } catch (error) {}
};

module.exports = {
  roleAuthSuperAdmin,
  roleAuth,
};
