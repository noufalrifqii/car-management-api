const jwt = require("jsonwebtoken");

const authJWT = (req, res, next) => {
  const authHeader = req.headers["authorization"];

  if (authHeader) {
    const secretKey = process.env.JWT_KEY;
    const token = authHeader.split(" ")[1];
    jwt.verify(token, secretKey, (err, user) => {
      if (err) {
        return res.status(403).json({
          message: err.message,
        });
      }
      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};

module.exports = authJWT;
