# Cars Management API - Chapter 6 - Muhammad Noufal Rifqi Iman

Sebuah website dengan management API di dalamnya.

## Deskripsi

Project ini merupakan final project untuk chapter 6 di Binar Academy program FullStack Web Development. Pada project ini kami melakukan managemen API di dalamnya. Di mana pada managemen ini terdapat 3 role yang memiliki akses tersendiri, yaitu Super Admin, Admin dan Member.

## Persyaratan

Sebelum masuk ke proses instalasi dan penggunaan, diperlukan untuk melakukan instalasi beberapa kebutuhan seperti di bawah ini :

- [Node.js](https://nodejs.org/)
- [NPM (Package Manager)](https://www.npmjs.com/)
- [Postman](https://www.postman.com/)
- [PostgreSQL](https://www.postgresql.org/)

## Instalasi dan Penggunaan

- Clone repository ini <br />
  ```bash
  git clone https://gitlab.com/noufalrifqii/car-management-api
  ```
- Download semua package yang dibutuhkan dan depedenciesnya <br />
  ```bash
  npm install
  ```
- Ubah isi file .env dan sesuaikan username dan password database sesuai milik anda
  ```bash
  USERNAME_DB_DEV = (your username),
  PASSWORD_DB_DEV = (your password)
  ```
- Buat Database
  ```bash
  sequelize db:create
  ```
- Jalankan Migration dan Seeder
  ```bash
  sequelize db:migrate
  sequelize db:seed
  ```
- Jalankan server
  ```bash
  npm start
  ```
- Buka postman dan jalankan endpoint yang tertera pada file api.js di folder routes

## Tech Stack

- Node.js 16.14 (Runtime Environment)
- Express 4.17.3 (HTTP Server)
- Sequelize 6.19.0 (ORM)
- PostgreSQL 14.2 (DBMS)
- Nodemon 2.0.16 (Server Runner)
- Bcryptjs 2.4.3 (Encryption)
- JSON Web Tokens 8.5.1 (Token For Request)
- Swagger UI Express 4.3.0 (Open API Documentation)

## Info Penting

Email dan Password untuk Superadmin

```bash
email: superNorzen@gmail.com
password: admin123#
```

Endpoint untuk Open API Document

```bash
http://localhost:3031/api-docs/
```
