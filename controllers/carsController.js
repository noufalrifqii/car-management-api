const car = require("../services/carsService");
const user = require("../services/usersService");
const jwt_decode = require("jwt-decode");
const db = require("../models");
const User = db.tbl_users;
const Car = db.tbl_cars;

async function getProfile(req, res) {
  try {
    const token = req.headers["authorization"];
    const splitToken = token.split(" ")[1];
    const decodeToken = jwt_decode(splitToken);
    const idUser = decodeToken.id;

    const data = await User.findOne({
      where: { id: idUser },
      attributes: { exclude: ["password"] },
    });
    return data;
  } catch (error) {}
}

async function addCars(req, res) {
  try {
    let user = await getProfile(req, res);
    let cars = req.body;

    let car = {
      name: cars.name,
      type: cars.type,
      deletedAt: null,
      availableAt: new Date(),
      createdBy: user.name,
      deletedBy: null,
      updatedBy: null,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    Car.create(car).then(() => {
      res.status(201).json({
        message: "Cars was saved!",
        data: car,
      });
    });
  } catch (error) {
    console.log(error);
  }
}

async function getAllCars(req, res) {
  try {
    let car = await Car.findAll({
      where: {
        deletedAt: null,
      },
    });
    res.status(200).json({
      message: "success",
      data: car,
    });
  } catch (error) {
    console.log(error);
  }
}

async function UpdateCars(req, res) {
  console.log("masuk");
  try {
    let cars = req.body;
    let user = await getProfile(req, res);
    let id = req.params.id;

    let car = {
      name: cars.name,
      type: cars.type,
      deletedAt: null,
      availableAt: new Date(),
      createdBy: cars.createdBy,
      deletedBy: null,
      updatedBy: user.name,
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    Car.update(car, {
      where: { id: id },
    }).then(() => {
      res.status(200).json({
        message: "Data berhasil diubah!",
        data: car,
      });
    });
  } catch (error) {
    console.log(error);
  }
}
async function softDeleteCars(req, res) {
  try {
    let user = await getProfile(req, res);
    const id = Number(req.params.id);
    let cars = {
      deletedAt: new Date(),
      deletedBy: user.name,
    };
    Car.update(cars, {
      where: { id: id },
    })
      .then(() => {
        res.status(200).json({
          message: "success",
          data: cars,
        });
      })
      .catch((error) => {
        res.status(500).json({
          message: error.message,
        });
      });
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

module.exports = {
  addCars,
  UpdateCars,
  getAllCars,
  softDeleteCars,
};
