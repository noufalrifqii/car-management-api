const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");
const user = require("../services/usersService");
const db = require("../models");
const User = db.tbl_users;

async function currentUser(req, res) {
  try {
    const token = req.headers["authorization"];
    const splitToken = token.split(" ")[1];
    const decodeToken = jwt_decode(splitToken);
    const idUser = decodeToken.id;

    const data = await User.findOne({
      where: { id: idUser },
      attributes: { exclude: ["password"] },
    });
    res.json({
      data,
    });
  } catch (error) {}
}

async function authUser(req, res) {
  try {
    const body = req.body;

    if (body.email !== "" && body.password !== "") {
      const user = await User.findOne({
        where: {
          email: body.email,
        },
      });
      let salt = "Ajndi";
      const matchPass = await bcrypt.compare(body.password + salt, user.password);
      if (!matchPass) {
        return res.status(401).json({
          message: "email/password is not match!",
        });
      }

      let jwtKey = process.env.JWT_KEY;
      const expireKey = "2h";
      const tokenAccess = jwt.sign(
        {
          id: user.id,
          email: user.email,
          name: user.name,
          role: user.role,
        },
        jwtKey,
        {
          algorithm: "HS256",
          expiresIn: expireKey,
        }
      );
      return res.status(200).json({
        message: "Success!",
        token: tokenAccess,
      });
    }
  } catch (error) {
    console.log(error);
  }
}

async function registerAdmin(req, res) {
  try {
    const body = req.body;
    let salt = "Ajndi";
    const hashPassword = await bcrypt.hash(body.password + salt, 12);

    let dataReq = {
      name: body.name,
      email: body.email,
      password: hashPassword,
      role: "admin",
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    User.create(dataReq)
      .then((data) => {
        res.status(200).json({
          message: "success",
        });
      })
      .catch((error) => {
        res.status(500).json({
          message: error,
        });
      });
  } catch (error) {
    console.log(error);
  }
}

async function deleteAdmin(req, res) {
  try {
    let idAdmin = req.params.id;
    User.destroy({ where: { id: idAdmin, role: "admin" } })
      .then(() => {
        res.status(200).json({
          message: "Data Berhasil Dihapus!",
          data: [],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  } catch (error) {
    console.log(error);
  }
}

async function registerUser(req, res) {
  try {
    const body = req.body;
    let salt = "Ajndi";
    const hashPassword = await bcrypt.hash(body.password + salt, 12);

    let dataReq = {
      name: body.name,
      email: body.email,
      password: hashPassword,
      role: "member",
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    User.create(dataReq)
      .then((data) => {
        res.status(200).json({
          message: "success",
        });
      })
      .catch((error) => {
        res.status(500).json({
          message: error,
        });
      });
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  currentUser,
  authUser,
  registerAdmin,
  deleteAdmin,
  registerUser,
};
