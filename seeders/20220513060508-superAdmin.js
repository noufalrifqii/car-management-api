"use strict";
const bcrypt = require("bcryptjs");

module.exports = {
  async up(queryInterface, Sequelize) {
    let salt = "Ajndi";
    const hashpassword = await bcrypt.hash("admin123#" + salt, 12);
    await queryInterface.bulkInsert(
      "tbl_users",
      [
        {
          name: "SuperNorzen",
          email: "superNorzen@gmail.com",
          password: hashpassword,
          role: 0,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
