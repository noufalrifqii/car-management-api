var express = require("express");
var router = express.Router();
const { UpdateCars, addCars, getAllCars, softDeleteCars } = require("../controllers/carsController");
const { currentUser, authUser, registerAdmin, deleteAdmin, registerUser } = require("../controllers/usersController");
const jwtAuth = require("../middlewares/jwtAuth");
const adminAuth = require("../middlewares/adminAuth");

// All users can access
router.get("/login", authUser);
router.get("/showCars", jwtAuth, getAllCars);
router.get("/currentUser", jwtAuth, currentUser);

// Only Admins and Super Admins
router.post("/admin/createCars", [jwtAuth, adminAuth.roleAuth], addCars);
router.put("/admin/deleteCars/:id", [jwtAuth, adminAuth.roleAuth], softDeleteCars);
router.put("/admin/updateCars/:id", [jwtAuth, adminAuth.roleAuth], UpdateCars);

// Only Super Admins
router.post("/admin/addAdmins", [jwtAuth, adminAuth.roleAuthSuperAdmin], registerAdmin);
router.delete("/admin/deleteAdmins/:id", [jwtAuth, adminAuth.roleAuthSuperAdmin], deleteAdmin);

// Only Member
router.post("/member/registration", registerUser);

module.exports = router;
