require("dotenv").config();

module.exports = {
  development: {
    username: process.env.USERNAME_DB_DEV,
    password: process.env.PASSWORD_DB_DEV,
    database: process.env.NAME_DB_DEV,
    host: process.env.HOST_DB_DEV,
    dialect: process.env.TYPE_DB_DEV,
  },
  test: {
    username: process.env.USERNAME_DB_TEST,
    password: process.env.PASSWORD_DB_TEST,
    database: process.env.NAME_DB_TEST,
    host: process.env.HOST_DB_TEST,
    dialect: process.env.TYPE_DB_TEST,
  },
  production: {
    username: process.env.USERNAME_DB_PROD,
    password: process.env.PASSWORD_DB_PROD,
    database: process.env.NAME_DB_PROD,
    host: process.env.HOST_DB_PROD,
    dialect: process.env.TYPE_DB_PROD,
  },
};
