const carsRepo = require("../repositories/carsRepo");

module.exports = {
  create(requestBody) {
    return carsRepo.create(requestBody);
  },

  update(requestBody, id) {
    return carsRepo.update(requestBody, id);
  },

  destroy(id) {
    return carsRepo.destroy(id);
  },

  findAll(condition) {
    return carsRepo.findAll(condition);
  },

  get(id) {
    return carsRepo.find(id);
  },
};
