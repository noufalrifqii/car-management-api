const usersRepo = require("../repositories/usersRepo");

module.exports = {
  create(requestBody) {
    return usersRepo.create(requestBody);
  },

  destroy(id) {
    return usersRepo.destroy(id);
  },

  findOne(condition) {
    return usersRepo.findOne(condition);
  },
};
