const { User } = require("../models/tbl_users");

module.exports = {
  create(createArgs) {
    return User.create(createArgs);
  },

  destroy(id) {
    return User.destroy(id);
  },

  findOne(condition) {
    return User.findOne(condition);
  },
};
