const { Car } = require("../models/tbl_cars");

module.exports = {
  create(createArgs) {
    return Car.create(createArgs);
  },

  update(updateArgs, id) {
    return Car.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  destroy(id) {
    return Car.destroy(id);
  },

  find(id) {
    return Car.findByPk(id);
  },

  findAll(condition) {
    return Car.findAll(condition);
  },
};
